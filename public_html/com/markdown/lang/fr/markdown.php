<?php
define('MARKDOWN_WIDGET_MARKDOWN_TITLE','Contenu au format MarkDown');
define('MARKDOWN_WIDGET_MARKDOWN_DESC','Permet d\'ajouter du contenu de type MarkDown');
define('MARKDOWN_WIDGET_MARKDOWN_FILE','Contenu depuis un fichier MD');
define('MARKDOWN_WIDGET_MARKDOWN_TEXT','Copier-coller le code MarkDown');
?>
