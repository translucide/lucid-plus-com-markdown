<?php
/**
 * Markdown content widget
 *
 * Sept. 08, 2018
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/data');
$service->get('Ressource')->get('com/markdown/lang/'.$service->get('Language')->getCode().'/markdown');
$service->get('Ressource')->get('core/widget/blockwidget');

class MarkdownWidget extends BlockWidget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'markdown',
			'type' => 'static',
			'name' => 'markdown',
			'title' => MARKDOWN_WIDGET_MARKDOWN_TITLE,
			'description' => MARKDOWN_WIDGET_MARKDOWN_DESC,
			'wireframe' => 'markdown',
			'icon' => 'markdown',
            'saveoptions' => array_merge($this->getOptionsNames(),array('mdfile','mdtext')),
			'copyoptions' => [
				'data' => [
					'data' => [
						'select' => [
							'data_type' => '{parentname}item',
							'data_parent' => '{parentobjid}'
						],
						'update' => [
							'data_parent' => '{parentobjid}'
						]
					]
				]
			]
		));
	}

	public function render(){
		global $service;
        $opt = $this->data->getVar('widget_options');
        $service->get('Ressource')->get('com/markdown/class/parsedown');
        $parser = new Parsedown();
        $mdtext = $opt['mdtext'];

        if ($opt['mdfile']) {
            $opt['mdfile'] = str_replace(UPLOADURL,'',$opt['mdfile']);
            header('mdfile: '.$opt['mdfile']);
        }

        //MD file ending in .md or zip file containing all images with a .md file in it.
        if ($opt['mdfile'] && file_exists(UPLOADROOT.$opt['mdfile']))  {

            //ZIP file uploaded...
            if (substr(strtolower($opt['mdfile']),-4,4) == '.zip') {
                $zip = new ZipArchive;
                $res = $zip->open(UPLOADROOT.$opt['mdfile']);
                if ($res === TRUE) {
                    array_map('unlink', glob(UPLOADROOT.'markdown/widget/markdown_'.$this->data->getVar('widget_objid')."/*.*"));
                    rmdir($dirname);
                    mkdir(UPLOADROOT.'markdown/widget/markdown_'.$this->data->getVar('widget_objid'),0777,true);
                    $zip->extractTo(UPLOADROOT.'markdown/widget/markdown_'.$this->data->getVar('widget_objid'));
                    $zip->close();

                    $extractdir = UPLOADROOT.'markdown/widget/markdown_'.$this->data->getVar('widget_objid');
                    $extracturl = UPLOADURL.'markdown/widget/markdown_'.$this->data->getVar('widget_objid');
                    $files = $service->get('Ressource')->listDir($extractdir);

                    //Find MD file.
                    $mdFile = null;
                    foreach($files as $k => $v ) {
                        if(substr(strtolower($v), -3, 3) == '.md') {
                            $mdFile = $v;
                            break;
                        }
                    }
                    $mdtext .= file_get_contents($extractdir.'/'.$mdFile);
                    unlink($extractdir.'/'.$mdFile);
                    foreach($files as $k => $v) {
                        if(substr(strtolower($v), -3, 3) != '.md') {
                            $mdtext = str_replace("(./$v)","($extracturl/$v)",$mdtext);
                        }
                    }
                }

            }
            else $mdtext .= file_get_contents(UPLOADROOT.$opt['mdfile']);
        }
        $content .= $parser->text($mdtext);
        return str_replace('{content}',$content,parent::render());
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');
        $form->add(new TextareaFormField('mdtext',$options['mdtext'],array(
			'tab'=> 'basic',
			'title' => MARKDOWN_WIDGET_MARKDOWN_TEXT,
			'width' => 9,
            'rows' => 12
		)));
		$form->add(new FileuploadFormField('mdfile',$options['mdfile'],array(
			'tab'=> 'basic',
			'title' => MARKDOWN_WIDGET_MARKDOWN_FILE,
			'width' => 3
		)));
		return parent::edit($objs,$form);
	}
}
?>
